package ru.tsc.bagrintsev.tm.api.controller;

import java.io.IOException;

public interface ITaskController {

    void showTaskList();

    void clearTasks();

    void createTask() throws IOException;

    void removeTaskByIndex() throws IOException;

    void removeTaskById() throws IOException;

    void showTaskByIndex() throws IOException;

    void showTaskById() throws IOException;

    void updateTaskByIndex() throws IOException;

    void updateTaskById() throws IOException;

}
