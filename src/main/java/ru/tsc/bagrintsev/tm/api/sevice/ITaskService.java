package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name);

    Task create(String name, String description);

    Task add(Task task);

    List<Task> findAll();

    Task findOneByIndex(Integer index);

    Task findOneById(String id);

    Task updateByIndex(Integer index, String name, String description);

    Task updateById(String id, String name, String description);

    Task removeTaskByIndex(Integer index);

    Task removeTaskById(String id);

    Task remove(Task task);

    void clear();

}
