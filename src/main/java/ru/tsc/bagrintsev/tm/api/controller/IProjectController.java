package ru.tsc.bagrintsev.tm.api.controller;

import java.io.IOException;

public interface IProjectController {

    void showProjectList();

    void clearProjects();

    void createProject() throws IOException;

    void removeProjectByIndex() throws IOException;

    void removeProjectById() throws IOException;

    void showProjectByIndex() throws IOException;

    void showProjectById() throws IOException;

    void updateProjectByIndex() throws IOException;

    void updateProjectById() throws IOException;

}
