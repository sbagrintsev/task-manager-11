package ru.tsc.bagrintsev.tm.api.repository;

import ru.tsc.bagrintsev.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    List<Project> findAll();

    Project findOneByIndex(Integer index);

    Project findOneById(String id);

    Project remove(Project project);

    Project removeByIndex(Integer index);

    Project removeById(String id);

    int projectCount();

    void clear();
}
