package ru.tsc.bagrintsev.tm.component;

import ru.tsc.bagrintsev.tm.api.controller.ICommandController;
import ru.tsc.bagrintsev.tm.api.controller.IProjectController;
import ru.tsc.bagrintsev.tm.api.controller.ITaskController;
import ru.tsc.bagrintsev.tm.api.repository.ICommandRepository;
import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.sevice.ICommandService;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectService;
import ru.tsc.bagrintsev.tm.api.sevice.ITaskService;
import ru.tsc.bagrintsev.tm.controller.CommandController;
import ru.tsc.bagrintsev.tm.controller.ProjectController;
import ru.tsc.bagrintsev.tm.controller.TaskController;
import ru.tsc.bagrintsev.tm.repository.CommandRepository;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;
import ru.tsc.bagrintsev.tm.service.CommandService;
import ru.tsc.bagrintsev.tm.service.ProjectService;
import ru.tsc.bagrintsev.tm.service.TaskService;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

import static ru.tsc.bagrintsev.tm.constant.CommandLineConst.*;
import static ru.tsc.bagrintsev.tm.constant.InteractionConst.*;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) throws IOException {
        processOnStart(args);

        commandController.showWelcome();
        initData();
        while (true) {
            System.out.println();
            System.out.println("Enter Command:");
            System.out.print(">> ");
            final String command = TerminalUtil.nextLine();
            processOnTheGo(command);
        }
    }

    private void initData() {
        taskService.create("first task", "task simple description");
        taskService.create("second task", "task simple description");
        taskService.create("third task", "task simple description");
        taskService.create("fourth task", "task simple description");

        projectService.create("first project", "project simple description");
        projectService.create("second project", "project simple description");
        projectService.create("third project", "project simple description");
        projectService.create("fourth project", "project simple description");
    }

    private void processOnStart(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case HELP_SHORT:
                commandController.showHelp();
                break;
            case VERSION_SHORT:
                commandController.showVersion();
                break;
            case ABOUT_SHORT:
                commandController.showAbout();
                break;
            case INFO_SHORT:
                commandController.showSystemInfo();
                break;
            case ARGUMENTS_SHORT:
                commandController.showArguments();
                break;
            case COMMANDS_SHORT:
                commandController.showCommands();
                break;
            default:
                commandController.showOnStartError(arg);
                commandController.showHelp();
        }
    }

    private void processOnTheGo(final String command) throws IOException {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case HELP:
                commandController.showHelp();
                break;
            case VERSION:
                commandController.showVersion();
                break;
            case ABOUT:
                commandController.showAbout();
                break;
            case INFO:
                commandController.showSystemInfo();
                break;
            case EXIT:
                commandController.close();
                break;
            case ARGUMENTS:
                commandController.showArguments();
                break;
            case COMMANDS:
                commandController.showCommands();
                break;
            case TASK_LIST:
                taskController.showTaskList();
                break;
            case TASK_CREATE:
                taskController.createTask();
                break;
            case TASK_CLEAR:
                taskController.clearTasks();
                break;
            case PROJECT_LIST:
                projectController.showProjectList();
                break;
            case PROJECT_CREATE:
                projectController.createProject();
                break;
            case PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            default:
                commandController.showError(command);
                commandController.showHelp();
        }
    }

    private void processOnStart(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        processOnStart(arg);
        commandController.close();
    }

}
