package ru.tsc.bagrintsev.tm.model;

import static ru.tsc.bagrintsev.tm.constant.CommonConst.*;

public final class Command {

    private String argumentName;

    private String commandName;

    private String description;

    public Command(String argumentName, String commandName, String description) {
        this.argumentName = argumentName == null ? EMPTY : argumentName;
        this.commandName = commandName == null ? EMPTY : commandName;
        this.description = description == null ? EMPTY : description;
    }

    public String getArgumentName() {
        return argumentName;
    }

    public void setArgumentName(String argumentName) {
        this.argumentName = argumentName == null ? EMPTY : argumentName;
    }

    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName == null ? EMPTY : commandName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? EMPTY : description;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("[");
        result.append(String.format("%-25s", argumentName));
        result.append(" | ");
        result.append(String.format("%-25s", commandName));
        result.append("]");
        if (!description.isEmpty()) {
            while (result.length() < 60) {
                result.append(" ");
            }
            result.append(description);
        }

        return result.toString();
    }

}
