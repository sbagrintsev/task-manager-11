package ru.tsc.bagrintsev.tm;

import ru.tsc.bagrintsev.tm.component.Bootstrap;

import java.io.IOException;

/**
 * @author Sergey Bagrintsev
 * @version 1.11.0
 */

public final class Application {

    public static void main(final String[] args) throws IOException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
