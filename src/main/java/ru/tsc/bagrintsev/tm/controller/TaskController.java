package ru.tsc.bagrintsev.tm.controller;

import ru.tsc.bagrintsev.tm.api.controller.ITaskController;
import ru.tsc.bagrintsev.tm.api.sevice.ITaskService;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTaskList() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() throws IOException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() throws IOException {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeTaskByIndex(index);
        if (task == null) {
            System.out.println("FAIL");
        }
        else {
            System.out.println("OK");
        }
    }

    @Override
    public void removeTaskById() throws IOException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeTaskById(id);
        if (task == null) {
            System.out.println("FAIL");
        }
        else {
            System.out.println("OK");
        }
    }

    @Override
    public void showTaskByIndex() throws IOException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        showTask(task);
        System.out.println("OK");
    }

    @Override
    public void showTaskById() throws IOException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        showTask(task);
        System.out.println("OK");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    @Override
    public void updateTaskByIndex() throws IOException {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) {
            System.out.println("FAIL");
        }
        else {
            System.out.println("OK");
        }
    }

    @Override
    public void updateTaskById() throws IOException {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateById(id, name, description);
        if (task == null) {
            System.out.println("FAIL");
        }
        else {
            System.out.println("OK");
        }
    }

}
