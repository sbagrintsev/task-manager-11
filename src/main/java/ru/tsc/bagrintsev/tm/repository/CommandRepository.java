package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.api.repository.ICommandRepository;
import ru.tsc.bagrintsev.tm.constant.CommandLineConst;
import ru.tsc.bagrintsev.tm.constant.InteractionConst;
import ru.tsc.bagrintsev.tm.model.Command;

import static ru.tsc.bagrintsev.tm.constant.CommonConst.EMPTY;

public final class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(
            CommandLineConst.HELP_SHORT, InteractionConst.HELP, "Print help.");

    public static final Command ABOUT = new Command(
            CommandLineConst.ABOUT_SHORT, InteractionConst.ABOUT, "Print about.");

    public static final Command VERSION = new Command(
            CommandLineConst.VERSION_SHORT, InteractionConst.VERSION, "Print version.");

    public static final Command INFO = new Command(
            CommandLineConst.INFO_SHORT, InteractionConst.INFO, "Print system info.");

    public static final Command EXIT = new Command(
            EMPTY, InteractionConst.EXIT, "Close program.");

    public static final Command ARGUMENTS = new Command(
            CommandLineConst.ARGUMENTS_SHORT, InteractionConst.ARGUMENTS, "Print command-line arguments.");

    public static final Command COMMANDS = new Command(
            CommandLineConst.COMMANDS_SHORT, InteractionConst.COMMANDS, "Print application interaction commands.");

    public static final Command TASK_LIST = new Command(
            null, InteractionConst.TASK_LIST, "Print task list.");

    public static final Command TASK_CREATE = new Command(
            null, InteractionConst.TASK_CREATE, "Create new task.");

    public static final Command TASK_CLEAR = new Command(
            null, InteractionConst.TASK_CLEAR, "Remove all tasks.");

    public static final Command PROJECT_LIST = new Command(
            null, InteractionConst.PROJECT_LIST, "Print project list.");

    public static final Command PROJECT_CREATE = new Command(
            null, InteractionConst.PROJECT_CREATE, "Create new project.");

    public static final Command PROJECT_CLEAR = new Command(
            null, InteractionConst.PROJECT_CLEAR, "Remove all projects.");

    public static final Command TASK_SHOW_BY_INDEX = new Command(
            null, InteractionConst.TASK_SHOW_BY_INDEX, "Show task by index.");

    public static final Command TASK_SHOW_BY_ID = new Command(
            null, InteractionConst.TASK_SHOW_BY_ID, "Show task by id.");

    public static final Command TASK_UPDATE_BY_INDEX = new Command(
            null, InteractionConst.TASK_UPDATE_BY_INDEX, "Update task by index.");

    public static final Command TASK_UPDATE_BY_ID = new Command(
            null, InteractionConst.TASK_UPDATE_BY_ID, "Update task by id.");

    public static final Command TASK_REMOVE_BY_INDEX = new Command(
            null, InteractionConst.TASK_REMOVE_BY_INDEX, "Remove task by index.");

    public static final Command TASK_REMOVE_BY_ID = new Command(
            null, InteractionConst.TASK_REMOVE_BY_ID, "Remove task by id.");

    public static final Command PROJECT_SHOW_BY_INDEX = new Command(
            null, InteractionConst.PROJECT_SHOW_BY_INDEX, "Show project by index.");

    public static final Command PROJECT_SHOW_BY_ID = new Command(
            null, InteractionConst.PROJECT_SHOW_BY_ID, "Show project by id.");

    public static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            null, InteractionConst.PROJECT_UPDATE_BY_INDEX, "Update project by index.");

    public static final Command PROJECT_UPDATE_BY_ID = new Command(
            null, InteractionConst.PROJECT_UPDATE_BY_ID, "Update project by id.");

    public static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            null, InteractionConst.PROJECT_REMOVE_BY_INDEX, "Remove project by index.");

    public static final Command PROJECT_REMOVE_BY_ID = new Command(
            null, InteractionConst.PROJECT_REMOVE_BY_ID, "Remove project by id.");


    private final Command[] commands = new Command[]{
            HELP, ABOUT, VERSION, INFO,
            ARGUMENTS, COMMANDS,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            TASK_SHOW_BY_INDEX, TASK_SHOW_BY_ID, TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID, TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_ID,
            PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_ID, PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID, PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_ID,
            EXIT
    };

    @Override
    public Command[] getAvailableCommands() {
        return commands;
    }
}
